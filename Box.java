/**
 * Team Name: Five Guys
 * Project Name: Burger Factory
 * Date: 2/25/2018
 * File: Box.java
 * Source of Help: 
 *        https://docs.oracle.com/javase/8/javase-clienttechnologies.htm
 *        Revel Introduction to Java Programming, Chapter 14-15
 *        class piazza
 *
 *   This class *TODO*
 */

// libraries
import java.util.Random;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene; 
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage; 
import java.io.FileInputStream;

/**
 * Name: Box
 * Purpose: *TODO*
 */

public class Box extends StackPane{

  // instance variables
 // private double r;    // radius
  private int x, y; // location
  //private Random ran = new Random();

  // constructor
  public void Box() {
    x = 0;
    y = 0;

    Image box = new Image("/box.png");
    ImageView boxView = new ImageView();//box
    boxView.setFitHeight(60);
    boxView.setFitWidth(90);
    boxView.setImage(box);

    this.getChildren().add(boxView);
  }

  // *TODO
  public void setLocation(int tempX, int tempY) {
    x = tempX;
    y = tempY;
  }

  // *TODO
  public void display() {
    /*
    Image image = new Image(new FileInputStream("box.png"));
    ImageView imageView = new ImageView(image);
    imageView.setX(x-50 + ran.nextInt(15));
    imageView.setY(y-100);     
    imageView.setFitHeight(110); 
    imageView.setFitWidth(110);  
    */ 
  }


  // A function that returns true or false based on
  // if the catcher intersects a raindrop
  /*
 public boolean intersect(Burger d) {
    // Calculate distance
    int distance = dist(x, y, d.x, d.y); 

    // Compare distance to sum of radii
    if (distance < r + d.r) { 
      return true;
    } else {
      return false;
    }
  }*/
}