/**
 * Team Name: Five Guys
 * Project Name: Burger Factory
 * Date: 2/25/2018
 * File: Burger.java
 * Source of Help: 
 *        https://docs.oracle.com/javase/8/javase-clienttechnologies.htm
 *        Revel Introduction to Java Programming, Chapter 14-15
 *        class piazza
 *
 *   This class *TODO*
 */

//need to import files of burger for display
//The initial burger icon stays stationary while the rows of random burger boxes and mouse move across the screen.  

// libraries
import java.io.*;
import java.util.*;
import javafx.scene.image.*; // image view
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Name: Burger
 * Purpose: *TODO*
 */

public class Burger extends StackPane{

  //i instance variables
  private int x_loc = 400; // location of burger, starts at  middle
  private int y_loc = -5; // starts a bit above frame
  private int speed = 1;  // speed of the burger falling, starts at 1
  private int faster = 5; // speed increment every 10 points
  private int frameHeight = 600;

  // New variable to keep track of whether drop is still being used
  boolean finished = false;

  // constructor
  public void Burger() {
    Image burgerPic = new Image("/burger.png");
    ImageView burgerView = new ImageView(); // burger
    burgerView.setFitHeight(40);
    burgerView.setFitWidth(40);
    burgerView.setImage(burgerPic);

    this.getChildren().add(burgerView);
  }
  
  // controls the burger's fall with the speed
  public void move() {
    y_loc += speed;
  }

  // incremet speed 
  public void speed() {
    speed += faster;
  }

  // check if the burger missed the box and touched the bottom
  public boolean landed() {
    if ( y_loc > frameHeight) 
      return true;
    else
      return false;
  }

}