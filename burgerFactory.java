/**
 * Team Name: Five Guys
 * Project Name: Burger Factory
 * Date: 2/25/2018
 * File: BurgerFactory.java
 * Source of Help: 
 *        https://docs.oracle.com/javase/8/javase-clienttechnologies.htm
 *        Revel Introduction to Java Programming, Chapter 14-15
 *        class piazza
 *        stackoverflow
 *
 *   This class *TODO*
 */

// libraries
import java.io.*;
import java.util.*;
import javafx.application.*;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.*; // image view
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*; // for color
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.*;

/**
 * Name: BurgerFactory
 * Purpose: 
 */

public class burgerFactory extends Application {

  // instance variables
  private Pane pane;
  private Label scoreNum;  
  private Image bgPic = new Image("/background.png");
  private int score = 0;
  private boolean isBarPressed;
  private AnimationTimer timerBurger;

  private MyKeyHandler keyHandler = new MyKeyHandler();

  // launch the program
  public static void main(String[] args) {
    Application.launch(args);
  }

  @Override // Override the og start method in Application class
  public void start(Stage burgerShop) {
    setUpPane(burgerShop);
  }
  
  // *TODO
  public void setUpPane(Stage burgerShop) {

    // create image display
    ImageView bgView = new ImageView(); // background
    bgView.setImage(bgPic);

    // display image
    BorderPane root1 = new BorderPane(); //the root pane
    root1.getChildren().add(bgView);
    Scene scene = new Scene( root1, 500, 200); 

    scoreNum = new Label("Score: ");
    scoreNum.setFont(Font.font (java.awt.Font.MONOSPACED, FontWeight.BOLD, 30));
    scoreNum.setTextFill(Color.BLACK);

    Label scoreShow = new Label("Score: ");
    scoreShow.setFont(Font.font("MONOSPACED", FontWeight.BOLD, 30));
    scoreShow.setTextFill(Color.BLACK);
    scoreNum.setFont(Font.font("MONOSPACED", FontWeight.BOLD, 30));
    scoreNum.setTextFill(Color.BLACK);
    updateScore(0);

    HBox displayScore = new HBox();
    displayScore.getChildren().addAll(scoreShow, scoreNum);

    root1.setRight(displayScore);
    root1.setMargin(displayScore, new Insets(30, 20, 20, 20));

    Image box = new Image("/box.png");
    ImageView boxView = new ImageView();//box
    boxView.setFitHeight(90);
    boxView.setFitWidth(110);
    boxView.setImage(box);

    Image mouse = new Image("/mouse.png");
    ImageView mouseView = new ImageView();//mouse
    mouseView.setFitHeight(70);
    mouseView.setFitWidth(90);
    mouseView.setImage(mouse);

    Image burgerPic = new Image("/burger.png");
    ImageView burgerView = new ImageView(); // burger
    burgerView.setFitHeight(40);
    burgerView.setFitWidth(40);
    burgerView.setImage(burgerPic);

    root1.getChildren().add(boxView);
    root1.getChildren().add(mouseView);
    root1.getChildren().add(burgerView);

    AnimationTimer timerBox = new AnimationTimer(){
      @Override
      public void handle(long now) {
        boxView.setX(boxView.getX() + 5.0);
        boxView.setY(470);
        if(boxView.getX() > 1000)
          boxView.setX(0);                                      
      }
    };
    timerBox.start();

    AnimationTimer timerMouse = new AnimationTimer(){
      @Override
      public void handle(long now) {
        double speed = 7.0;
        if(mouseView.getX() < 0)
          mouseView.setX(1000); 
        mouseView.setX(mouseView.getX() - speed);
        mouseView.setY(400);
      }
    };
    timerMouse.start();

   burgerView.setFocusTraversable(true);
   burgerView.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event) {
                switch(event.getCode()) {
                    case SPACE: isBarPressed = true; break;
                    //in case of further game development can add more keys
                }
            }
        });
/*
    burgerView.setOnKeyReleased(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event) {
                switch(event.getCode()) {
                    case SPACE: isBarPressed = false; break;
                    //in case of further game development can add more keys
                }
            }
        });
        */

    AnimationTimer timerBurger = new AnimationTimer(){
      @Override
      public void handle(long now) {
        if(isBarPressed){
          burgerView.setX(150);
          burgerView.setY(burgerView.getY() + 5.0);
          if (burgerView.getY() > 570){
            burgerView.setY(0);
            isBarPressed = false;
          }
          //score +1 if touch box
          if ( (burgerView.getY() <= (boxView.getY()+30)) &&
              (burgerView.getY() >= (boxView.getY()-30)) ) {
            if ( (burgerView.getX() <= (boxView.getX()+30)) &&
                (burgerView.getX() >= (boxView.getX()-30)) ) {
              burgerView.setY(0);
              isBarPressed = false;
              update(score+1);
            }
          }
          //game over if touch mouse
          if ( (burgerView.getY() <= (mouseView.getY()+30)) &&
              (burgerView.getY() >= (mouseView.getY()-30)) ) {
            if ( (burgerView.getX() <= (mouseView.getX()+30)) &&
                (burgerView.getX() >= (mouseView.getX()-30)) ) {
              gameOver(burgerView);
            }
          }
        }                                    
      }
    };
    timerBurger.start();

    burgerShop.setTitle("Welcome to the Five Guys Burger Shop!");
    burgerShop.setWidth(1000);
    burgerShop.setHeight(570);
    burgerShop.setScene(scene);  
    burgerShop.show();
  }

  public void updateScore(int num) {
    score = num;
    String newScore = Integer.toString(score);
    scoreNum.setText(newScore);
  }

  public void update(int sc){
    updateScore(sc);
  }

  class MyKeyHandler implements EventHandler<KeyEvent> {
  // handle the keys pressed by the user
    public void handle( KeyEvent e) {

      // if user hits space
      if (e.getCode() == KeyCode.SPACE) {
      // creat a new burger that falls from the top of the screen
        System.out.println("Spacebar Pressed!");
        Burger bun = new Burger();
      }
    }
  }

  public void gameOver(ImageView bun) {
    Text GG = new Text("GAME OVER");
    GG.setFont(Font.font("Times New Roman", FontWeight.BOLD, 50));
    GG.setFill(Color.BLACK);
    //Create a borderpane so i can put things on
    BorderPane gameOver = new BorderPane();
    Scene scene1 = new Scene(gameOver);
    Stage primaryStage1 = new Stage();
    //Title of the window
    primaryStage1.setTitle("Game Over!!!");
    primaryStage1.setScene(scene1);
    //Put text on the borderpane
    gameOver.setCenter(GG);
    //set size for the window
    primaryStage1.setWidth(400);
    primaryStage1.setHeight(400);
    //show the window
    primaryStage1.show();
    //clear score
    this.update(0);
    isBarPressed = false;
    bun.setY(0);
  }
}