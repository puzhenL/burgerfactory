Team: Five Guys
Project Name: The Five Guys Burger Shop

*TODO*
	We 

Burger Factory Game Rules:
	1. At the start of the game, there will be a random row of burger boxes and mouse moving across the bottom of the screen, aka the supply line. 
	2. When the player hit on the spacebar a burger will fall down from the initial burger icon to the supply line. 
	3. The goal is to make the burgers fall on to every burger box without falling onto a mice.
	4. As the player successully puts more and more burgers into burger boxes, the speed of the burger falling will automatically increase.

Scoring: 
	1. Each time a burger falls on a burger box one point is awarded to the player. 
	2. Points are accumulated on the top right hand corner.  
	3. The game is over either when a burger falls on a mice or a burger box passed through without a burger in it. 