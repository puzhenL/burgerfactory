/**
 * Team Name: Five Guys
 * Project Name: Burger Factory
 * Date: 2/25/2018
 * File: Mouse.java
 * Source of Help: 
 *        https://docs.oracle.com/javase/8/javase-clienttechnologies.htm
 *        Revel Introduction to Java Programming, Chapter 14-15
 *        class piazza
 *
 *   This class *TODO*
 */

// libraries
import java.util.Random;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene; 
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage; 
import java.io.FileInputStream;

/**
 * Name: Mouse
 * Purpose: *TODO*
 */

public class Mouse extends StackPane{

  // instance variables
 // double r;    // radius
  private int x, y; // location
  //Random ran = new Random();

  // constructor
  public void Mouse() {
    x = 0;
    y = 0;

    Image mouse = new Image("/mouse.png");
    ImageView mouseView = new ImageView();//mouse
    mouseView.setFitHeight(90);
    mouseView.setFitWidth(110);
    mouseView.setImage(mouse);

    this.getChildren().add(mouseView);
  }

  // *TODO
  public void setLocation(int tempX, int tempY) {
    x = tempX;
    y = tempY;
  }

  // A function that returns true or false based on
  // if the catcher intersects a raindrop
  /*
  public boolean intersect(Burger d) {
    // Calculate distance
    int distance = dist(x, y, d.x, d.y); 

    // Compare distance to sum of radii
    if (distance < r + d.r) { 
      return true;
    } else {
      return false;
    }
  }*/
}