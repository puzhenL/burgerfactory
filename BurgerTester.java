/**
 * Team Name: Five Guys
 * Project Name: Burger Factory
 * Date: 2/25/2018
 * File: BurgerTester.java
 * Source of Help: 
 *        https://docs.oracle.com/javase/8/javase-clienttechnologies.htm
 *        Revel Introduction to Java Programming, Chapter 14-15
 *        class piazza
 *
 *   This class is a tester for the BurgerFactory and try to catch and handle 
 *   exceptions
 */

/**
 * Name: BurgerTester
 * Purpose: catch and handle exceptions for the BurgerFactory
 */

 public class BurgerTester {

 	public static void main(String[] args) throws Exception {
 		System.out.println("========== Testing BurgerFactory ==========\n");

 		int count = 0;

 		if (count > 0) {
 			System.out.println("!!!!! Test Failed !!!!!\n");
 		}
 		else {
 			System.out.println("!!!!! Test Passed !!!!!\n");
 		}

 		System.out.println("========== End of Test ==========");
 	}

 	// *TODO*
 	// more methods to test for exceptions in the BurgerFactory class
 	// and other classes
 }